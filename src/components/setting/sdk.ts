import SDK from "casdoor-js-sdk";

export const sdkConfig = {
    serverUrl: "https://sso.lab.linksafe.vn",
    clientId: "9a67ec6eea935509f491",
    clientSecret: "c6d47cc06a489deb737ee5110b43d4fd1fe7af3b",
    organizationName: "sso-organization",
    appName: "sso_1",
    redirectPath: "/login",
    signinPath: "/api/signin",
  }; 

// export const sdkConfig = {
//   serverUrl: "https://sso.lab.linksafe.vn",
//   clientId: "336889d64fcf08150fff",
//   clientSecret: "5214c79e475d97e400e61d3c6beb67c4bcc20b94",
//   organizationName: "sso-organization",
//   appName: "sso_2",
//   redirectPath: "/login",
//   signinPath: "/api/signin",
// }; 


// export const sdkConfig = {
//   serverUrl: "https://sso.lab.linksafe.vn",
//   clientId: "f4e4e003c8635e908795",
//   clientSecret: "1b82af2d57dc4539f096e46d4f659600ebecf2b1",
//   organizationName: "sso-organization",
//   appName: "sso_3",
//   redirectPath: "/login",
//   signinPath: "/api/signin",
// }; 



export const onGetCasdoorSDK = () => {
  const sdk = new SDK(sdkConfig)
  return sdk
};