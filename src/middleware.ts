import { NextResponse } from "next/server";

export default function middleware(req: any) {
  //protected route
  const protectedRoutes = ["/profile"];
  const casdoorUserCookie = req.cookies.get("casdoorUser");
  const isAuthenticated = casdoorUserCookie ? true : false;

  //Authentication Function
  if (!isAuthenticated && protectedRoutes.includes(req.nextUrl.pathname)) {
    return NextResponse.redirect(new URL("/login", req.url));
  }
}


