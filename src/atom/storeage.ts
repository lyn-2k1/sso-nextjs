import { atomWithStorage, createJSONStorage } from 'jotai/utils'

let storage = createJSONStorage(() => {
  if (global?.localStorage) {
    return localStorage
  }
  return null as any
})

export const atomAuthToken = atomWithStorage<any>('atomAuthToken', {}, storage)