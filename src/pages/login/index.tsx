import Icon from "@ant-design/icons";
import { Button, Card, Space, Typography } from "antd";
import { logo } from "components/icon";
import { sdkConfig } from "components/setting/sdk";
import { useAtom } from "jotai";
import SDK from "casdoor-js-sdk";

import React, { useEffect, useState } from "react";
import { TokenReceivedAtom, userInforAtom } from "atom/atom-base";
import { useRouter } from "next/router";

const LoginPage: React.FC = (props: any) => {
  const [sdk, setSdk] = useState<any>();
  const [tokenReceived, setTokenReceived] = useAtom(TokenReceivedAtom);
  const [, setUserInfo] = useAtom(userInforAtom);
  const [loading, setLoading] = useState<boolean>(false);

  const router = useRouter();

  useEffect(() => {
    if (typeof window !== "undefined") {
      const newSDK = new SDK(sdkConfig);
      setSdk(newSDK);
      if (window.location.href.indexOf("code") !== -1) {
        if (!sessionStorage.getItem("token")) {
          setLoading(true);
          newSDK.signin("/api").then((res: any) => {
            sessionStorage.setItem("token", res.token);
            setTokenReceived(true);
            if (sessionStorage.getItem("token")) {
              setLoading(false);
              router.push("/home"); 
            }
          });
         
        }
      }
    }
  }, []);

  useEffect(() => {
    if (sessionStorage.getItem("token")) {
      router.push("/home");
    }
  }, [tokenReceived]);

  // async function getInfo() {
  //   setLoading(true);
  //   let token = sessionStorage.getItem("token");
  //   if (!token) {
  //     return;
  //   } else {
  //     return fetch(`/api/getUserInfo?token=${token}`)
  //       .then((res) => res.json())
  //       .then((res) => {
  //         setUserInfo(res);
  //         if (res) {
  //           router.push("/home");
  //         }
  //         setLoading(false);
  //       });
  //   }
  // }

  const onFinish = (values: any) => {
    window.location.href = sdk.getSigninUrl();
  };

  const onSignUp = () => {
    window.location.href = sdk.getSignupUrl();
  };

  return (
    <div style={{ marginTop: "20vh", textAlign: "center" }}>
      <Icon component={() => <img style={{ width: 80 }} src={logo} />} />
      <Typography.Title level={2}>{"Sign in with Lancs SSO "}</Typography.Title>
      <Typography.Text type="secondary">
        {"Please log in through your Lancs SSO account here"}
      </Typography.Text>
      <Card style={{ width: "500px", margin: "auto", marginTop: "30px" }}>
        {/* <LoginForm onFinish={onFinish} /> */}
        <Space.Compact direction="vertical">
          <Button
            style={{ width: "400px", borderRadius: "10px" }}
            onClick={onFinish}
            size="large"
            type="primary"
            loading={loading}
          >
            Login with Lancs SSO
          </Button>
          <Button type="link" onClick={onSignUp}>
            Sign up now
          </Button>
        </Space.Compact>
      </Card>
    </div>
  );
};

export default LoginPage;
