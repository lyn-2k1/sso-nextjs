// import "@src/styles/globals.css";
import { ConfigProvider, theme } from "antd";
import { color } from "components/icon";
import { Provider } from "jotai";
import type { AppProps } from "next/app";
// import theme from "./theme/themeConfig";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Provider>
      <ConfigProvider  theme={{
      token: {
        colorPrimary: color,
      },
    }}>
      <Component {...pageProps} />
      </ConfigProvider>
    </Provider>
  );
}
