import { useAtom } from "jotai";

import LoginPage from "./login";
import Home from "./home";
import { useEffect, useState } from "react";

const Page = () => {
  const [token, setToken] = useState<any>();

  useEffect(() => {
    if (typeof window !== "undefined") {
      const getToken = sessionStorage?.getItem("token");
      console.log("getToken", getToken)
      setToken(getToken);
    }
  }, []);

  if (token) {
    return <Home />;
  }
  return <LoginPage />;
};

export default Page;
