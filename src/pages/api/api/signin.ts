import { NextApiRequest, NextApiResponse } from "next";
import { authCfg } from "../setting";
import { SDK } from "casdoor-nodejs-sdk";
import { parse } from "url";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const sdk = new SDK(authCfg);
  if (req.method === "POST") {
    const { code } = parse(req.url || "", true).query;
    if (!code) {
      return res.status(400).json({ error: "Code is required" });
    }

    try {
      const response = await sdk.getAuthToken(code as string);
      const accessToken = response.access_token;
      return res.status(200).json({ token: accessToken });
    } catch (error) {
      return res.status(500).json({ error: "Failed to authenticate" });
    }
  } else {
    return res.status(405).json({ error: "Method Not Allowed" });
  }
}
