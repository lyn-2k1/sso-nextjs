import { sdkConfig } from "components/setting/sdk";

export const cert = `
-----BEGIN CERTIFICATE-----
MIIE7zCCAtegAwIBAgIDAeJAMA0GCSqGSIb3DQEBCwUAMDExGTAXBgNVBAoTEHNz
by1vcmdhbml6YXRpb24xFDASBgNVBAMMC2NlcnRfbTR0dDhoMB4XDTI0MDQxMTAz
NDYwM1oXDTQ0MDQxMTAzNDYwM1owMTEZMBcGA1UEChMQc3NvLW9yZ2FuaXphdGlv
bjEUMBIGA1UEAwwLY2VydF9tNHR0OGgwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAw
ggIKAoICAQCvAGJmSMR29tiUq8FclSJU94y/BunpowzKi0/+7mgYc47cfZc90n/T
WhV6g7I/r4+yL3dNo9hKNxMLGO09jARmzHiOYhWCpJ5he6ki7LhdVZAHyd/bwVo8
f5G6vdI2s2xq4SKLjjMw5BMpUcKjoVAQ28rpgTKEmYAq+dmPfr9WX+sis5FImNad
Pq9XNwkz+QQcVyoi+Pw3+Dk7Dj5/i2kjjb7Y/AMF6ZZUB719tCgVZtvIbe/xMtcE
nyzQ45q77d/YIW8uKhsKuG3t27O5YxqzSho+C8bggNLWICMOB20sIprHgexs81tb
eaScZ5Twe3hOVItVFpLwAOWu8V0bBHQKUizwiRfmPQGYcFHDXcHIG/Kmj+tQyCV/
rzph1i9dNepaUCxzNAIvHMpuTv2DWx+jPwLxpSHzcUmlAp3/GwdnHnHeo/wTpT4k
WtHK5/7Eb1nBgLCmOeOZCIzoIND462I3kKlh9be/lc+QFytVCyn0fAwtD/Q3n1ML
2KVdc4kpIVPDYlh4x3ZMuNvUPidh0jfFPq+ugO2y4r8J3WfWeVxCXSJYbXWRpn8p
ec2Eu0Xt1avhZhSMPoeNldY3fIDPTgV25Dr0XA9jyojEFOpq6o4y3apUZar9NqfM
6qVUYJ6qJxkFOIjW0KARW8qQVOaj26rgUrBVAaoEAw6r/wNnW4TQkQIDAQABoxAw
DjAMBgNVHRMBAf8EAjAAMA0GCSqGSIb3DQEBCwUAA4ICAQBLHk13aI0iTMWb84Ip
bboWxbI1V7gKpDN2YrnzgdHU2pu8T9Xe/BcD+k7C3gPq+2t6uUH+kjelVvs6xT7e
mywUxUewoDRfB8kAlSEJJ8kJYmzmnq7k2vJwDWSM+/aXL9MrWhRuFxviZr9D9SGf
sj4+tn1467bkA/pIjeX58YAidSIoYzdxRtjgFsVLJQuS6+e6vWgMAV/iwHE0QuRO
ataPbkRQpmL3e4JVP6vJPoLDXKu29y/BpxlmBv1sCfYNMsT9eXxdtg5F1EXYq260
yBSu6ctV6GzkRvu708GYgEJoMxbnjjeMDGjZojYUwavfh60VUZ635Y7JSHbiygaz
6MPHrw/T019kL1aLDG+2tZ7Eg/pFfHXdpzD3mSR6coKHdcYAX8DAh33BekKZy2BR
ZAfcwnstuZZtyR+Zt8lLOh+TzPBxSbkjFi+rrFjISQsewckxoottahZCMmcjl69V
cqapwjytsgbgqrm/SpPCa3+Ggs/MBrrJ5ma7tlmLuIBXSH3847RHYxfjIssbDENJ
08Kgc6/SLjrJt/QGYwywbZmA5knb4fgykKR8NcQo04Bs4NPTJWkmuQOK134aHbcA
D3DXOkKIw8KECKIb2CmiStZ0b13pQQ0UbQe7CZjKTp4oWxKjdMallnvUFUJ6/GCQ
OPay10IJGc8cAUIVZiUJAgqKDw==
-----END CERTIFICATE-----
`;

export const authCfg = {
  endpoint: sdkConfig.serverUrl,
  clientId: sdkConfig.clientId,
  clientSecret: sdkConfig.clientSecret,
  certificate: cert,
  orgName: sdkConfig.organizationName,
  appName: sdkConfig.appName,
}
