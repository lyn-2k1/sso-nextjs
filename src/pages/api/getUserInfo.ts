import { parse } from "url";
import { NextApiRequest, NextApiResponse } from "next";
import { authCfg } from "./setting";
import { SDK } from "casdoor-nodejs-sdk";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const sdk = new SDK(authCfg);
  if (req.method === 'GET') {
    const { token } = req.query;
    if (!token) {
      return res.status(400).json({ error: 'Token is required' });
    }

    try {
      const user = sdk.parseJwtToken(token as string);
      return res.status(200).json(user);
    } catch (error) {
      return res.status(500).json({ error: 'Failed to parse token' });
    }
  } else {
    return res.status(405).json({ error: 'Method Not Allowed' });
  }
}
