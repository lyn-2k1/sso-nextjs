import fs from 'fs';
import path from 'path';
import type { NextApiRequest, NextApiResponse } from "next";


export default function handler(req: NextApiRequest,
    res: NextApiResponse<any>,) {
  fs.readFile(path.resolve(__dirname, '../index.html'), (err, data) => {
    if (err) {
      res.status(500).send('Error reading index.html');
    } else {
      res.setHeader('Content-Type', 'text/html');
      res.send(data);
    }
  });
}