import {
  Button,
  Card,
  Descriptions,
  Space,
  Typography,
  message as messAntd,
} from "antd";
import { useAtom } from "jotai";

import SDK from "casdoor-js-sdk";

import React, { useEffect, useState } from "react";
import { sdkConfig } from "components/setting/sdk";
import Icon from "@ant-design/icons";
import { useRouter } from "next/router";
import { color, logo, nameApp } from "components/icon";
import { TokenReceivedAtom, userInforAtom } from "atom/atom-base";
import { atomAuthToken } from "atom/storeage";

const Home = () => {
  const [userInfo, setUserInfo] = useAtom(userInforAtom);
  const [sdk, setSdk] = useState<any>();
  const [, setTokenReceived] = useAtom(TokenReceivedAtom);

  const [accessToken, setAccessToken] = useAtom(atomAuthToken);

  const router = useRouter();

  function signOut() {
    sessionStorage.removeItem("token");
    setTokenReceived(false);
    setAccessToken("")
    router.push("/login");
  }

  useEffect(() => {
    if (typeof window !== "undefined") {
      const newSDK = new SDK(sdkConfig);
      setSdk(newSDK);
    }

    console.log("userInfo", userInfo);
    if (!userInfo?.name) {
      getInfo();
    }
  }, []);

  async function getInfo() {
    let token = sessionStorage.getItem("token");
    if (!token) {
      return;
    } else {
      return fetch(`/api/getUserInfo?token=${token}`)
        .then((res) => res.json())
        .then((res) => {
          console.log("res", res);
          setUserInfo(res);
          // if (res) {
          //   router.push("/home");
          // }
        });
    }
  }

  const onAcount = () => {
    window.location.href = sdk.getMyProfileUrl();
  };

  return (
    <Card
      style={{ width: "100vw" }}
      title={
        <Space>
          <Icon
            component={() => <img style={{ width: "30px" }} src={logo} />}
          />
          <Typography.Text style={{ color: color }}>{nameApp}</Typography.Text>
        </Space>
      }
      extra={
        <Space>
          <Button type="primary" onClick={onAcount}>
            My account
          </Button>

          <Button onClick={signOut} type="primary" danger>
            Logout
          </Button>
        </Space>
      }
    >
      <Descriptions title="User Information">
        <Descriptions.Item label="UserName">{userInfo?.name}</Descriptions.Item>
        <Descriptions.Item label="Avatar">
          <Icon component={() => <img width={50} src={userInfo?.avatar} />} />
        </Descriptions.Item>
      </Descriptions>
      <img src="/sso1.jpg" style={{ width: "100%", height: "auto" }} />
      {/* <img src="/sso2.jpg" style={{ width: "100%", height: "auto" }} /> */}
    </Card>
  );
};

export default Home;
